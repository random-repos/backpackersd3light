
// NOTE: If warnings appear, you may need to retarget this project to .NET 4.0. Show the Solution
// Pad, right-click on the project node, choose 'Options --> Build --> General' and change the target
// framework to .NET 4.0 or .NET 4.5.

namespace fsharpmono
open UnityEngine
type FSharpMono() = class
    inherit MonoBehaviour()
    
    //Position and Rotation
    let mutable forwardRotation : Quaternion = Quaternion.identity
    let mutable distance = 5.
    let mutable hammerPosition : Vector3 = new Vector3((float32)0,(float32)0,(float32)0)
    let mutable hammerLastPosition : Vector3 = Vector3.zero
    
    //GameObjects
    let mutable fakeBody : GameObject = null
    member public this.hammerImage : GameObject = null
    member public this.body : GameObject = null
    member public this.hammer : GameObject = null
    
    //Utility nonsense
    member this.OffsetMousePosition = 
        Input.mousePosition - new Vector3((float32)Screen.width,(float32)Screen.height,(float32)0) / (float32)2.
    
    member this.Start = 
        Debug.Log("starting fsharpmono")
        fakeBody <- new GameObject("genFakeHammer")
        fakeBody.transform.parent <- this.hammer.transform
        
    member this.Update = 
        hammerLastPosition <- this.hammer.transform.position
        this.move_real_body
        this.move_hammer_mouse
        this.move_hammer_key
        
    member this.move_real_body =
        this.body.rigidbody.AddForce(this.hammer.transform.position - this.body.transform.position, ForceMode.Impulse)
    
    member this.move_hammer_mouse = 
        hammerPosition <-  hammerPosition + this.OffsetMousePosition
        distance <- (float)hammerPosition.magnitude
        forwardRotation <- Quaternion.FromToRotation( Vector3.right, hammerPosition)
        
    member this.move_hammer_key = 
        let rotate = 
            match () with 
            | () when Input.GetKeyDown(KeyCode.RightArrow) -> (float32)1
            | () when Input.GetKeyDown(KeyCode.LeftArrow) -> (float32)(-1)
            | _ -> (float32)0
        let move = 
            match () with 
            | () when Input.GetKeyDown(KeyCode.UpArrow) -> (float32)1
            | () when Input.GetKeyDown(KeyCode.DownArrow) -> (float32)(-1)
            | _ -> (float32)0
        this.hammer.rigidbody.AddRelativeForce(Vector3.forward * move)
        this.hammer.rigidbody.AddTorque(Vector3.up * rotate * (float32)(-1),ForceMode.Impulse)
        let current = this.hammer.transform.position - fakeBody.transform.position
        this.hammer.rigidbody.AddForce(Quaternion.AngleAxis(rotate, Vector3.up) * current - current, ForceMode.Impulse)
        
        
end