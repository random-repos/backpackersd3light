using UnityEngine;
using System.Collections;

public class SexyController : FakeMonoBehaviour {
    public SexyController(ManagerManager aManager) : base(aManager) { }

    //input
    InputManager.MouseEventHandler mMouseEventHandler = new InputManager.MouseEventHandler();

    //gameObjects
    GameObject body = null;
    GameObject hammerImage = null;
    GameObject hammer = null;
    HammerBehaviour hammerBehaviour = null;
    HammerBehaviour bodyHammerBehaviour = null;

    //camera
    Camera cam = null;

    float forwardRotation = 0;
    float distance = 2;
    
    //mouse stuff TODO rename
    Vector3 hammerPosition;
    Vector3 lastHammerPosition;

    Vector3 bodyDesiredPosition;

    //TODO delete htis doneneed it
    Vector3 hammerCollidedPosition;

    Vector3 excludeNext = Vector3.zero;
    Vector3 preMouseMove = Vector3.zero;


    public Vector3 offset_mouse_position()
    {
        return Input.mousePosition - (new Vector3(Screen.width,Screen.height,0) / 2.0f);
    }
	public override void Start () {
        mMouseEventHandler.mMousePressed += mouse_pressed;
        
        hammerImage = mManager.hammerImage;
        body = mManager.body;
        hammer = mManager.hammer;
        bodyHammerBehaviour = hammer.GetComponent<HammerBehaviour>();
        hammerBehaviour = hammer.GetComponent<HammerBehaviour>();

        cam = (new GameObject("genCamera")).AddComponent<Camera>();

        //position everything properly and reset the velocities
        
        body.rigidbody.velocity = Vector3.zero;
        set_mouse_input();
        move_body();
        body.transform.position = body.transform.position;
        body.rigidbody.velocity = Vector3.zero;
        hammerCollidedPosition = hammer.transform.position;
	}

    public void set_camera()
    {
        cam.transform.position = body.transform.position + hammer.transform.forward * -5;
        cam.transform.LookAt(body.transform);
    }
	
    public override void FixedUpdate()
    {
        hammer.rigidbody.velocity = Vector3.zero; //kill velocities from last frame, we will recompute these so called velocities
        hammer.rigidbody.angularVelocity = Vector3.zero;
        //body.rigidbody.velocity = Vector3.zero;


        //respond hammer (accounts for velocity ofbody)
        move_hammer();
        collide_hammer();
        move_body();

        check_gravity();

        //set the hammer image
        hammerImage.transform.position = hammer.transform.position;
        if((hammer.transform.position-lastHammerPosition).sqrMagnitude > 0.01f)
            hammerImage.transform.rotation = Quaternion.Slerp(hammer.transform.rotation, Quaternion.FromToRotation(Vector3.right, hammer.transform.position - lastHammerPosition), 0.3f);
        lastHammerPosition = hammer.transform.position; //update for hammer image

        set_camera();
	}

    public Vector3 compute_position_change()
    {
        Vector3 temp = hammer.transform.position;
        hammer.transform.position = hammerCollidedPosition;
        body.transform.parent = hammer.transform;
        Vector3 op = body.transform.position;
        body.transform.localPosition = cam.convert_units((cam.transform.position - body.transform.position).magnitude,new Vector3(-Mathf.Sin(forwardRotation), -Mathf.Cos(forwardRotation), 0) * distance);
        Vector3 r = body.transform.position - op;
        body.transform.position = op;
        body.transform.parent = null;
        hammer.transform.position = temp;
        return r;
    }

    public void move_body()
    {
        Vector3 fakeBodyBaseChange = compute_position_change();
        body.rigidbody.velocity += fakeBodyBaseChange / Time.fixedDeltaTime;
        body.rigidbody.MovePosition(body.transform.position + fakeBodyBaseChange);
    }

    public void set_mouse_input()
    {
        Vector3 diff = offset_mouse_position() - hammerPosition;
        diff = diff.normalized * Mathf.Min(diff.magnitude, 1000 * Time.fixedDeltaTime);
        hammerPosition += diff;
        distance = Mathf.Clamp(hammerPosition.magnitude, 0, 400);
        forwardRotation = Mathf.Atan2(-hammerPosition.y, hammerPosition.x);
    }

    public void check_gravity()
    {
        RaycastHit hitInfo;
        bool collision = hammer.rigidbody.SweepTest(Vector3.down, out hitInfo, 0.1f);
        if (collision)
        {
            Physics.gravity = Vector3.zero;
        }
        else
            Physics.gravity = new Vector3(0, -15, 0);
    }

    public void collide_hammer()
    {
        RaycastHit hitInfo;
        bool collision = hammer.rigidbody.SweepTest(hammer.rigidbody.velocity.normalized, out hitInfo, hammer.rigidbody.velocity.magnitude * Time.fixedDeltaTime);
        if (collision)
        {
            hammerCollidedPosition = hammer.transform.position + hammer.rigidbody.velocity.normalized * hitInfo.distance;

            //a certain amount of this motion was due to body moving and we don't want thta to add velocity
            //body.rigidbody.velocity += preMouseMove / Time.fixedDeltaTime;

            /*excludeNext = hitInfo.normal.normalized;
            if (Vector3.Dot(body.rigidbody.velocity, excludeNext) > 0)
                body.rigidbody.velocity = Vector3.Exclude(excludeNext, body.rigidbody.velocity);*/
            body.rigidbody.velocity = Vector3.zero;
        }
        else
            hammerCollidedPosition = hammer.transform.position + hammer.rigidbody.velocity * Time.fixedDeltaTime;
        hammer.rigidbody.velocity = (hammerCollidedPosition - hammer.transform.position) / Time.fixedDeltaTime;
       
        
        //body.rigidbody.velocity = Vector3.Exclude(hitInfo.normal, body.rigidbody.velocity);
        
    }

    public void move_hammer()
    {

        preMouseMove = -compute_position_change();


        //mouse
        set_mouse_input();

        //keyboard
        float rotate = 0;
        if (Input.GetKey(KeyCode.RightArrow))
            rotate = 1;
        else if (Input.GetKey(KeyCode.LeftArrow))
            rotate = -1;
        float move = 0;
        if (Input.GetKey(KeyCode.UpArrow))
            move = 1;
        else if (Input.GetKey(KeyCode.DownArrow))
            move = -1;

        rotate *= 30 * Time.fixedDeltaTime;
        //hammer.rigidbody.MoveRotation(Quaternion.AngleAxis(-rotate, Vector3.up)*hammer.rigidbody.rotation);
        hammer.rigidbody.rotation = (Quaternion.AngleAxis(rotate, Vector3.up) * hammer.rigidbody.rotation);
        Vector3 current = hammer.transform.position - body.transform.position;
        //Vector3 keyMove = Quaternion.AngleAxis(rotate, Vector3.up) * current - current + hammer.transform.forward * move * Time.fixedDeltaTime;
        Vector3 keyMove = hammer.transform.forward * move * Time.fixedDeltaTime; //ugg, why dose this work??? oh i c... it odes

        //body.rigidbody.MoveRotation(Quaternion.AngleAxis(rotate, Vector3.up) * body.rigidbody.rotation);
        //body.rigidbody.rotation = (Quaternion.AngleAxis(rotate, Vector3.up) * body.rigidbody.rotation);
        //Vector3 keyMove = hammer.transform.forward * move * Time.fixedDeltaTime;
        
        Vector3 mouseMove = -compute_position_change();
        Vector3 totalMove = keyMove + mouseMove;

        
        /*
        Vector3 velMove = body.rigidbody.velocity*Time.fixedDeltaTime;
        Vector3 velOrth = Vector3.Exclude((totalMove).normalized, velMove);
        Vector3 velPar = velMove - velOrth;
        if (velPar.sqrMagnitude > totalMove.sqrMagnitude)
            totalMove = velPar;
         * */
        //totalMove += velOrth;
         

        //hammer.rigidbody.move(hammer.transform.position + totalMove);
        hammer.rigidbody.velocity = totalMove / Time.fixedDeltaTime;

        Debug.DrawRay(hammer.transform.position, hammer.rigidbody.velocity - (Quaternion.AngleAxis(rotate, Vector3.up) * current - current) / Time.fixedDeltaTime, new Color(1,0,0));
        Debug.DrawRay(hammer.transform.position, hammer.rigidbody.velocity);
    }


    public bool mouse_pressed(InputManager.MouseProfile aMouse){
        return true; //we want focus no matter where you click...
    }
}
