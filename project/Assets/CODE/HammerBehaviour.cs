using UnityEngine;
using System.Collections.Generic;

public class HammerBehaviour : MonoBehaviour {


    public List<Collision> collisions = new List<Collision>();

    public void clean_memory()
    {
        collisions.Clear();
    }
    public void OnCollisionStay(Collision e)
    {
        collisions.Add(e);
    }
}
